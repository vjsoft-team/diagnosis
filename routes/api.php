<?php

use Illuminate\Http\Request;

use App\Hospital;
use App\Doctor;
use App\TrfTest;
use App\patient;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/getHospitals', function (Request $request) {
    // return $request;
    $doctorId = $request->doctorId;
    $getDoctorDetails = Doctor::find($doctorId);
    $hospitalName = $getDoctorDetails['hospital'];
    $getHospital = Hospital::where('name', "$hospitalName")->get();

    return $getHospital;
});

Route::post('/getTestFormat', function (Request $request) {

    $testId = $request->testId;
    // return $testId;
    $getTestDetails = TrfTest::find($testId);
    $testReport = $getTestDetails['report'];
    // $getHospital = Hospital::where('hospitalName', "$hospitalName")->get();

    return $testReport;
});

Route::post('/updateReport', function (Request $request) {

    $testId = $request->updateId;
    $updateReportValue = $request->updateReportValue;
    // return $testId;
    $getTestDetails = TrfTest::find($testId);
    $trfId  = $getTestDetails['trfId'];
    $getTestDetails->report = $updateReportValue;
    $getTestDetails->status = 1;
    // tests count

    $getTestDetails->save();
    $count = DB::SELECT("SELECT count(*) count FROM trf_tests where trfId = '$trfId'");
    $trfCount = $count[0]->count;
    $countS = DB::SELECT("SELECT count(*) count FROM trf_tests where trfId = '$trfId' and status = 1");
    $reportsCompleted = $countS[0]->count;
    // return $reportsCompleted;
    if ($reportsCompleted > 0) {
      $updateTrf = DB::UPDATE("update trves set status = 1 where id = '$trfId'");
    }
    if ($trfCount == $reportsCompleted) {
      $updateTrf = DB::UPDATE("update trves set status = 2 where id = '$trfId'");
      $hospitalId = $getTestDetails['hospitalid'];
      $hospitalDetails = Hospital::find($hospitalId);
      $hospitalName = $hospitalDetails['name'];
      $hospitalNumber = $hospitalDetails['contactNumber'];
      $patientId = $getTestDetails['patientId'];
      $patientDetails = patient::find($patientId);
      $patientId = $patientDetails['id'];
      $patientName = $patientDetails['patientName'];
      $msg = "Dear $hospitalName, your TRF $trfId for $patientName has been completed. Please login to download your reports
              TRF ID: $trfId.
              PATIENT ID: $patientId
              Thank you";
      $url = "http://onlinebulksmslogin.com/spanelv2/api.php?username=shuklatrans&password=695152&to=$hospitalNumber&from=CHITSS&message=".urlencode($msg);
      $ere = file($url);
    }
    // $testReport = $getTestDetails['report'];
    // $getHospital = Hospital::where('hospitalName', "$hospitalName")->get();

    return $getTestDetails;
});
