<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
// Route::get('/invoice', function () {
//     return view('invoice');
// });

Auth::routes();
Route::post('/user/logout', 'Auth\LoginController@UserLogout')->name('user.logout');

Route::get('/home', 'HomeController@index')->name('home');

//Hospitals
Route::get('/addHospital', 'HomeController@addHospital');
Route::post('/addHospital','HomeController@store')->name('addHospital');
Route::get('/listHospital', 'HomeController@listHospital');
Route::get('/editHospital/{id}', 'HomeController@editHospital');
Route::post('/editHospitalUpdate', 'HomeController@update')->name('hospitalUpdate');
Route::get('/deleteHospital/{id}', 'HomeController@deleteView');
Route::post('/deleteHospital', 'HomeController@delete')->name('hospitalDelete');


//Doctors
Route::get('/addDoctor', 'DoctorController@addDoctor');
Route::post('/addDoctor','DoctorController@store')->name('addDoctor');
Route::get('/listDoctor', 'DoctorController@listDoctor');
Route::get('/editDoctor/{id}', 'DoctorController@editDoctor');
Route::post('/editDoctorUpdate', 'DoctorController@update')->name('doctorUpdate');
Route::get('/deleteDoctor/{id}', 'DoctorController@deleteView');
Route::post('/deleteDoctor', 'DoctorController@delete')->name('doctorDelete');

//Test
Route::get('/addTest', 'TestController@addTest');
Route::post('/addTest','TestController@store')->name('addTest');
Route::get('/listTest', 'TestController@listTest');
Route::get('/editTest/{id}', 'TestController@editTest');
Route::post('/editTestUpdate', 'TestController@update')->name('testUpdate');
Route::get('/deleteTest/{id}', 'TestController@deleteView');
Route::post('/deleteTest', 'TestController@delete')->name('testDelete');

// Trf
Route::get('/trf', 'TrfController@index');
Route::post('/trf', 'TrfController@store')->name('trfStore');

// Trf Report
Route::get('/trf-report', 'TrfController@reportTable');
Route::get('/trf-report-view/{id}', 'TrfController@reportView');
Route::get('/trf-add-report/{id}', 'TrfController@reportAddView');
Route::get('/trf-success/{id}', 'TrfController@success');

// Reports
Route::get('/trf-completed', 'TrfController@completedTrfs');
Route::get('/trf-pending', 'TrfController@pendingTrfs');



Route::prefix('admin')->group(function(){
  Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
  Route::post('/login','Auth\AdminLoginController@Login')->name('admin.login.submit');
  Route::get('/', 'AdminController@index')->name('admin.dashboard');
  Route::post('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
});

Route::prefix('account')->group(function(){
  Route::get('/login','Auth\AccountLoginController@showLoginForm')->name('account.login');
  Route::post('/login','Auth\AccountLoginController@Login')->name('account.login.submit');
  Route::get('/', 'AccountController@index')->name('account.dashboard');
  Route::get('/generate', 'AccountController@generate')->name('account.generate');
  Route::get('/generate/{id}', 'AccountController@generateHospital')->name('account.gnerateHospital');
  Route::get('/generate/invoice/{id}', 'AccountController@generateInvoice')->name('account.gnerateInvoice');
  Route::post('/price-change', 'AccountController@priceChange')->name('account.priceChange');
  Route::get('/trf-completed', 'TrfController@completedTrfs');
  Route::get('/pending-invoice', 'TrfController@pendingInvoice');
  Route::get('/invoice-hospital/{id}', 'TrfController@invoiceHospital');
  Route::post('/sendInvoice', 'AccountController@sendInvoice')->name('sendInvoice');
  Route::post('/payInvoice', 'AccountController@payInvoice')->name('payInvoice');
});

Route::prefix('hospital')->group(function(){
  Route::get('/login','Auth\HospitalLoginController@showLoginForm')->name('hospital.login');
  Route::post('/login','Auth\HospitalLoginController@Login')->name('hospital.login.submit');
  Route::get('/', 'HospitalController@index')->name('hospital.dashboard');
  Route::get('/reports', 'HospitalController@reportTable')->name('hospital.reports');;
  Route::get('/reports-completed', 'HospitalController@reportTableCompleted')->name('hospital.reports.completed');
  Route::get('/trf-report-view/{id}', 'HospitalController@reportView');
  Route::get('/test-report/{id}', 'HospitalController@testReportView');
});
