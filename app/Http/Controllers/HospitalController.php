<?php

namespace App\Http\Controllers;

use App\Hospital;
use App\trf;
use App\TrfTest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use Auth;

class HospitalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     //
     public function __construct()
     {
         $this->middleware('auth:hospital');
     }

     public function index()
     {
         return view('hospital/dashboard');
     }









    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    /**
     * Display the specified resource.
     *
     * @param  \App\Hospital  $hospital
     * @return \Illuminate\Http\Response
     */
    public function show(Hospital $hospital)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hospital  $hospital
     * @return \Illuminate\Http\Response
     */
    public function edit(Hospital $hospital)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hospital  $hospital
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hospital  $hospital
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hospital $hospital)
    {
        //
    }

    public function reportTable()
    {
      return view('hospital-report');
    }

    public function reportTableCompleted()
    {
      return view('trf-report-hospital');
    }

    public function reportView($id)
    {
      $trf = trf::find($id);
      return view('invoice-hospital')->with('data', $trf);
    }

    public function testReportView($id)
    {
      $trf = TrfTest::find($id);
      return view('report-hospital')->with('data', $trf);
    }
}
