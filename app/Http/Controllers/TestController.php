<?php

namespace App\Http\Controllers;

use App\Test;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function addTest()
     {
       return view('addTest');
     }

     public function store(Request $request)
     {
       $this->validate($request,[
       'name' => 'required',
       'code' => 'required',
       'price' => 'required',
       'report' => 'required',
       ]);



       $test = new Test();
       $test->name = $request->name;
       $test->code = $request->code;
       $test->price = $request->price;
       $test->report = $request->report;
       $test->save();

       return redirect('/listTest')->with('message', 'Test added Succesfully!');

     }

     public function listTest()
     {
       return view('listTest');
     }

     public function editTest($id)
     {
       $editview = Test::find($id);

       return view('editTest')->with('data', $editview);
     }
     public function update(Request $request)
     {
       // return $request;
       $this->validate($request,[
         'name' => 'required',
         'code' => 'required',
         'price' => 'required',
         'report' => 'required',
       ]);

       // Auth::user()->id;
       $data = Test::find($request->id);
       $data->name = $request->name;
       $data->code = $request->code;
       $data->price = $request->price;
       $data->report = $request->report;
       $data->save();

       return redirect('/listTest')->with('message', 'List Edited Succesfully!');
     }
     public function deleteView($id)
     {
       $deleteview = Test::find($id);

       return view('deleteTest')->with('data', $deleteview);
     }
     public function delete(Request $request)
     {

       $data = Test::find($request->id);
       $data->delete();
       // $data->dReason = $request->dReason;
       // $data->dUser = $dUser;
       // $data->uDate = $dDate;
       // $data->uTime = $dTime;
       // $data->save();


       return redirect('/listTest')->with('message', 'Test Deleted Succesfully!');
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    /**
     * Display the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function show(Test $test)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function edit(Test $test)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function destroy(Test $test)
    {
        //
    }
}
