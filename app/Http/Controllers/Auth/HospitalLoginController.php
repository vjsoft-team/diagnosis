<?php

namespace App\Http\Controllers\auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class HospitalLoginController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest:hospital');
    }

    public function showLoginForm()
    {
      return view('auth.hospital_login');
    }
    public function Login(Request $request)
    {
      $this->validate($request,[
        'email' => 'required|email',
        'password' => 'required|min:5'
      ]);
      if (Auth::guard('hospital')->attempt(['email'=> $request->email, 'password'=> $request->password], $request->remember)) {
        return redirect()->intended(route('hospital.dashboard'));
      }
      return redirect()->back()->withInput($request->only('email','remember'));
    }


}
