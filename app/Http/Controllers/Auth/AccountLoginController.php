<?php

namespace App\Http\Controllers\auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AccountLoginController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest:account');
    }

    public function showLoginForm()
    {
      return view('auth.account_login');
    }
    public function Login(Request $request)
    {
      $this->validate($request,[
        'email' => 'required|email',
        'password' => 'required|min:5'
      ]);
      if (Auth::guard('account')->attempt(['email'=> $request->email, 'password'=> $request->password], $request->remember)) {
        return redirect()->intended(route('account.dashboard'));
      }
      return redirect()->back()->withInput($request->only('email','remember'));
    }


}
