<?php

namespace App\Http\Controllers;

use App\Doctor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function addDoctor()
     {
       return view('addDoctor');
     }

     public function store(Request $request)
     {
       $this->validate($request,[
       'doctorName' => 'required',
       'contactNumber' => 'required',
       'hospital' => 'required',
       ]);

       $hospital = new Doctor();
       $hospital->doctorName = $request->doctorName;
       $hospital->contactNumber = $request->contactNumber;
       $hospital->hospital = $request->hospital;
       $hospital->save();

       return redirect('/listDoctor')->with('message', 'Doctor added Succesfully!');

     }
     public function listDoctor()
     {
       return view('listDoctor');
     }

     public function editDoctor($id)
     {
       $editview = Doctor::find($id);

       return view('editDoctor')->with('data', $editview);
     }
     public function update(Request $request)
     {
       $this->validate($request,[
       'doctorName' => 'required',
       'contactNumber' => 'required',
       'hospital' => 'required',
       ]);
       // return $request;
       // Auth::user()->id;
       $data = Doctor::find($request->id);
       $data->doctorName = $request->doctorName;
       $data->contactNumber = $request->contactNumber;
       $data->hospital = $request->hospital;
       $data->save();

       return redirect('/listDoctor')->with('message', 'Doctor Edited Succesfully!');
     }

     public function deleteView($id)
     {
       $deleteview = Doctor::find($id);

       return view('deleteDoctor')->with('data', $deleteview);
     }

     public function delete(Request $request)
     {

       $data = Doctor::find($request->id);
       $data->delete();
       // $data->dReason = $request->dReason;
       // $data->dUser = $dUser;
       // $data->uDate = $dDate;
       // $data->uTime = $dTime;
       // $data->save();


       return redirect('/listDoctor')->with('message', 'Doctor Deleted Succesfully!');
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * Display the specified resource.
     *
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function show(Doctor $doctor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function edit(Doctor $doctor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Doctor $doctor)
    {
        //
    }
}
