<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hospital;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware(['auth', 'auth:admin']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function addHospital()
    {
      return view('addHospital');
    }

    public function store(Request $request)
    {
      $this->validate($request,[
      'name' => 'required',
      'contactNumber' => 'required',
      'contactPerson' => 'required',
      'email' => 'required',
      'password' => 'required',
      'address' => 'required',
      ]);

      $hospital = new Hospital();
      $hospital->name = $request->name;
      $hospital->contactNumber = $request->contactNumber;
      $hospital->contactPerson = $request->contactPerson;
      $hospital->email = $request->email;
      $hospital->password = Hash::make($request->password);
      $hospital->address = $request->address;
      $hospital->save();

      return redirect('/listHospital')->with('message', 'Hospital added Succesfully!');
    }

    public function listHospital()
    {
      return view('listHospital');
    }

    public function editHospital($id)
    {
      $editview = Hospital::find($id);

      return view('editHospital')->with('data', $editview);
    }
    public function update(Request $request)
    {
      $this->validate($request,[
      'name' => 'required',
      'contactNumber' => 'required',
      'contactPerson' => 'required',
      'address' => 'required',
      ]);
      // return $request;
      // Auth::user()->id;
      if ($request->password == '') {
        $data = Hospital::find($request->id);
        $data->name = $request->hospitalName;
        $data->contactNumber = $request->contactNumber;
        $data->contactPerson = $request->contactPerson;
        $data->address = $request->address;
        $data->email = $request->email;
        $data->save();
      } else {
        $data = Hospital::find($request->id);
        $data->name = $request->hospitalName;
        $data->contactNumber = $request->contactNumber;
        $data->contactPerson = $request->contactPerson;
        $data->address = $request->address;
        $data->email = $request->email;
        $data->password = Hash::make($request->password);
        $data->save();
      }

      return redirect('/listHospital')->with('message', 'Hospital Edited Succesfully!');
    }

    public function deleteView($id)
    {
      $deleteview = Hospital::find($id);

      return view('deleteHospital')->with('data', $deleteview);
    }
    public function delete(Request $request)
    {

      $data = Hospital::find($request->id);
      $data->delete();
      // $data->dReason = $request->dReason;
      // $data->dUser = $dUser;
      // $data->uDate = $dDate;
      // $data->uTime = $dTime;
      // $data->save();


      return redirect('/listHospital')->with('message', 'Hospital Deleted Succesfully!');
    }
}
