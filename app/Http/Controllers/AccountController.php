<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\TrfTest;
use App\trf;
use App\Hospital;
use DB;
class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:account');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('account/dashboard');
    }

    public function generate()
    {
      return view('generateTest');
    }

    public function generateHospital($id)
    {
      return view('generateHospital')->with('hospitalid', $id);
    }

    public function generateInvoice($id)
    {
      return view('generateInvoice')->with('id', $id);
    }

    public function priceChange(Request $request)
    {
      $trfId = $request->trfId;
      $trfTest = TrfTest::where('trfId', $trfId)->get();
      $sno = 0;
      // for ($i=0; $i < count($trfTest); $i++) {
        foreach ($trfTest as $key) {
          $price = $request['price'][$sno++];
          // echo $price.'/';
          $testId = $key['id'];
          $update = DB::UPDATE("UPDATE trf_tests set price = $price WHERE id = $testId");
        }

        return back();
      // }
    }

    public function sendInvoice(Request $request)
    {
      $ids = implode(', ', $request['trfs']);
      // return $ids;
      $invoice = DB::INSERT("INSERT INTO `invoice` (trf_id, status) VALUES ('$ids', '1')");
      $invoiceId = DB::SELECT("SELECT id FROM invoice WHERE trf_id = '$ids' and status = 1");
      $invoiceId = $invoiceId[0]->id;
      // return $invoiceId;
      foreach ($request['trfs'] as $key) {
        // return dd($invoice);
        // return $invoiceId[0]->id;
        $trfUpdate = trf::find($key);
        $trfUpdate->status = 4;
        $trfUpdate->invoice_id = $invoiceId;
        $trfUpdate->save();
        $hospitalId = $trfUpdate['hospitalid'];
        $hospitalDetails = Hospital::find($hospitalId);
        // $msg = "Dear $request->name, your $request->chitValue Chit Account with GroupId $request->customerID is Created Successfully. And the total number of Emi's are $request->months ($emi/month).";
        $hospitalName = $hospitalDetails['name'];
        $hospitalNumber = $hospitalDetails['contactNumber'];
        $totalTrf = DB::SELECT("SELECT * FROM trf_tests WHERE trfId = $key");
        $tot = 0;
        foreach ($totalTrf as $price) {
          $tot += $price->price;
        }
        $msg = "Dear $hospitalName, you have a new invoice pending for a total amount of $tot.

        Thank you";
        $url = "http://onlinebulksmslogin.com/spanelv2/api.php?username=shuklatrans&password=695152&to=$hospitalNumber&from=CHITSS&message=".urlencode($msg);
        $ere = file($url);
      }

      return back();
    }

    public function payInvoice(Request $request)
    {
      // $ids = implode(', ', $request['trfs']);
      // // return $ids;
      // $invoice = DB::INSERT("INSERT INTO `invoice` (trf_id, status) VALUES ('$ids', '1')");
      // $invoiceId = DB::SELECT("SELECT id FROM invoice WHERE trf_id = '$ids' and status = 1");
      // $invoiceId = $invoiceId[0]->id;
      // return $invoiceId;
      foreach ($request['trfs'] as $key) {
        // return dd($invoice);
        // return $invoiceId[0]->id;
        $trfUpdate = trf::find($key);
        $trfUpdate->status = 5;
        // $trfUpdate->invoice_id = $invoiceId;
        $trfUpdate->save();
      }

      return back();
    }
}
