<?php

namespace App\Http\Controllers;

use App\trf;
use App\patient;
use App\TrfTest;
use App\Test;
use App\Hospital;
use Illuminate\Http\Request;

class TrfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('trf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $patient = new patient();
        $patient->patientName = $request->patientName;
        $patient->dob = $request->dob;
        $patient->age = $request->age;
        $patient->weight = $request->weight;
        $patient->uid = $request->uid;
        $patient->gender = $request->gender;
        $patient->address = $request->address;
        $patient->phone = $request->phone;
        $patient->height = $request->height;
        $patient->save();
        $patientId = $patient['id'];

        // $hospital =new Hospital();
        // $hospitaltId = $hospital['id'];
        // return $patient;
    

        $trf =  new trf();
        $trf->patientId = $patientId;
        $trf->doctorId = $request->doctorName;
        $trf->hospitalid = $request->hospitalid;
        $trf->hospitalCC = $request->hospitalCC_;
        $trf->drawnDate = $request->drawnDate;
        $trf->drawnTime = $request->drawnTime;
        $trf->receviedDate = $request->receviedDate;
        $trf->receviedTime = $request->receviedTime;
        $trf->oldPathgene = $request->oldPathgene;
        $trf->sp1 = $request->serum;
        $trf->sp2 = $request->plasma;
        $trf->sp3 = $request->aspirate;
        $trf->sp4 = $request->bloodAcd;
        $trf->sp5 = $request->bloodEdta;
        $trf->sp6 = $request->bloodFluoride;
        $trf->sp7 = $request->bloodSodiumCitrate;
        $trf->sp8 = $request->fluid;
        $trf->sp9 = $request->sputum;
        $trf->sp10 = $request->filterPaper;
        $trf->sp11 = $request->boneMarrow;
        $trf->sp12 = $request->csf;
        $trf->sp13 = $request->tissueSmallMedium;
        $trf->sp14 = $request->tissueLarge;
        $trf->sp15 = $request->parafinBlock;
        $trf->sp16 = $request->smear;
        $trf->sp17 = $request->sideHE;
        $trf->sp18 = $request->urine;
        $trf->sp19 = $request->bal;
        $trf->sp20 = $request->stool;
        $trf->sp21 = $request->swab;
        $trf->sp22 = $request->others;
        $trf->professionalDiagnosis = $request->proffesionalDiagnosis;
        $trf->hoMedication = $request->hoMedication_;
        $trf->medicationStatus = $request->status_;
        $trf->onGoingDuration = $request->onGoingDuration;
        $trf->terminatedWhen = $request->terminatedWhen;
        $trf->fatingPeriod = $request->fastingPeriod;
        $trf->HrUrineVolume = $request->urineVolume;
        $trf->LMP = $request->lmp;
        $trf->diabticStatus = $request->diabeticStatus_;
        $trf->geStation = $request->gestation_;
        $trf->tmpSentFrozen = $request->sFrozen;
        $trf->tmpSentReFregator = $request->sRefregerator;
        $trf->tmpSentAmbient = $request->sAmbient;
        $trf->tmpReceivedFrozen = $request->rFrozen;
        $trf->tmpReceivedReFregator = $request->rRefregerator;
        $trf->tmpReceivedAmbient = $request->rAmbient;
        $trf->save();
        $trfId = $trf['id'];

        foreach ($request->test as $test) {
          $TrfTest = new TrfTest();
          $resultFormat = Test::find($test);
          $TrfTest->trfId = $trfId;
          $TrfTest->testId = $test;
          $TrfTest->patientId = $patientId;
          $TrfTest->report = $resultFormat['report'];
          $TrfTest->name = $resultFormat['name'];
          $TrfTest->price = $resultFormat['price'];
          $TrfTest->save();

        }
        // return $request->test;

        $hospitalDetails = Hospital::find($request->hospitalid);
        // $msg = "Dear $request->name, your $request->chitValue Chit Account with GroupId $request->customerID is Created Successfully. And the total number of Emi's are $request->months ($emi/month).";
        $hospitalName = $hospitalDetails['name'];
        $hospitalNumber = $hospitalDetails['contactNumber'];
        $msg = "Dear $hospitalName, your test sample for $request->patientName has been received.
                TRF ID: $trfId.
                PATIENT ID: $patientId
                Thank you";
        $url = "http://onlinebulksmslogin.com/spanelv2/api.php?username=shuklatrans&password=695152&to=$hospitalNumber&from=CHITSS&message=".urlencode($msg);
        $ere = file($url);

        return redirect("/trf-success/$trfId");
    }

    public function reportTable()
    {
      $trfs = trf::orderBy('id', 'ASC')->get();
      // return $trfs;
      return view('trf-report');
    }

    public function reportView($id)
    {
      $trf = trf::find($id);
      return view('invoice')->with('data', $trf);
    }

    public function reportAddView($id)
    {
      return view('trf-add-report')->with('trfId', $id);
    }

    public function success($id)
    {
        return view('trf-success')->with('success', $id);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\trf  $trf
     * @return \Illuminate\Http\Response
     */
    public function show(trf $trf)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\trf  $trf
     * @return \Illuminate\Http\Response
     */
    public function edit(trf $trf)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\trf  $trf
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, trf $trf)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\trf  $trf
     * @return \Illuminate\Http\Response
     */
    public function destroy(trf $trf)
    {
        //
    }

    public function completedTrfs()
    {
      $completedTrfs = trf::where("status", 2)->get();
      return view('trf-completed-table')->with('data', $completedTrfs);
      // return $completedTrfs;
    }
    public function pendingInvoice()
    {
      // $completedTrfs = trf::where("status", 4)->get();
      return view('pending-invoice');
      // return $completedTrfs;
    }

    public function invoiceHospital($id)
    {
      // $completedTrfs = trf::where("status", 4)->get();
      return view('invoiceHospital')->with('hospitalid', $id);
      // return $completedTrfs;
    }

    public function pendingTrfs()
    {
      $completedTrfs = TrfTest::where("status", 0)->get();
      return view('trf-pending-table')->with('data', $completedTrfs);
      // return $completedTrfs;
    }
}
