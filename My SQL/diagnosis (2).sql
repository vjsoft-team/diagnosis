-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 10, 2018 at 03:23 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diagnosis`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'account', 'account@vjsoft.org', '$2y$10$WGlhg9FShRfkli/pQTYgou8otlxVQZF3ccQCLE7RdyC7W2ROrhkdy', NULL, '2018-09-01 05:03:08', '2018-09-01 05:03:08');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'vJ', 'vj@vjsoft.org', '$2y$10$f83KpcdurQphy7J1YYNeUuwg0AjR3WqVZnQHSQ5imHTgLdUVSxGuq', NULL, '2018-09-01 04:09:33', '2018-09-01 04:09:33');

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `id` int(10) UNSIGNED NOT NULL,
  `doctorName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactNumber` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hospital` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`id`, `doctorName`, `contactNumber`, `hospital`, `created_at`, `updated_at`) VALUES
(2, 'Raghu', '8056096998', 'Ramachandra', '2018-08-22 04:34:38', '2018-08-22 04:34:38'),
(4, 'Mithran', '99858899', 'Ramachandra', '2018-08-22 04:47:16', '2018-08-22 04:47:16'),
(5, 'test', '9490501349', 'Ramachandra Hospitals', '2018-08-26 05:00:37', '2018-08-26 05:00:37'),
(6, 'vj', '949050149', 'hospital', '2018-09-03 01:02:45', '2018-09-03 01:02:45'),
(7, 'Rajesh', '9865741588', 'SKP', '2018-09-03 04:00:56', '2018-09-03 04:00:56');

-- --------------------------------------------------------

--
-- Table structure for table `hospitals`
--

CREATE TABLE `hospitals` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contactPerson` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hospitals`
--

INSERT INTO `hospitals` (`id`, `name`, `email`, `contactNumber`, `contactPerson`, `address`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'hospital', 'hospital@vjsoft.org', '', NULL, '20-1-471/h5/g', '$2y$10$DAwn4u1mfZ8skEuXs73/kO.uyStvgATJmytRR4zLbvQ7/MK6s96..', NULL, '2018-09-02 23:26:55', '2018-09-02 23:26:55'),
(2, 'SKP', 'skp@vjsoft.org', '7878787', '877787778', 'sdsdsdsd', '$2y$10$8ceyUZXhsYhHcrQMyDfLCObP0hkQDs0A0TanEEs1YaJZCQkxBHjP.', NULL, '2018-09-03 02:32:13', '2018-09-03 02:32:13');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_08_22_055526_create_hospitals_table', 2),
(4, '2018_08_22_093251_create_doctors_table', 3),
(5, '2018_08_23_044235_create_tests_table', 4),
(6, '2018_08_25_031202_create_trves_table', 5),
(7, '2018_08_26_103443_create_patients_table', 6),
(8, '2018_08_26_111007_create_trf_tests_table', 7),
(10, '2018_09_01_092751_create_admins_table', 8),
(11, '2018_09_01_101739_create_accounts_table', 9),
(12, '2018_09_01_101812_create_hospitals_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `id` int(10) UNSIGNED NOT NULL,
  `patientName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `height` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trfId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`id`, `patientName`, `dob`, `age`, `weight`, `uid`, `gender`, `address`, `phone`, `height`, `trfId`, `created_at`, `updated_at`) VALUES
(1, 'Kumar', '6 September, 2018', '4', '25', '4156', 'F', 'No 12 Middle Street', '8959635278', '5', NULL, '2018-09-05 00:11:36', '2018-09-05 00:11:36'),
(2, 'Kaushik', '8 September, 2018', '6', '56', '65656', 'M', 'hhdjhdkjhf', '46446446', '4', NULL, '2018-09-05 00:51:42', '2018-09-05 00:51:42');

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `report` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `name`, `code`, `price`, `report`, `created_at`, `updated_at`) VALUES
(1, 'Jandis', 'GST567', '456', 'Duplicate Copy', '2018-08-23 00:42:08', '2018-08-23 01:47:27'),
(2, 'Maleria', 'GHY67', '500', 'Heart Beat Geeting High', '2018-08-23 00:44:12', '2018-08-23 01:48:05'),
(6, 'Typoid', '11', '11', 'Heavy Fever in Body', '2018-08-26 09:08:36', '2018-08-26 09:15:15');

-- --------------------------------------------------------

--
-- Table structure for table `trf_tests`
--

CREATE TABLE `trf_tests` (
  `id` int(10) UNSIGNED NOT NULL,
  `trfId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patientId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `report` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `name` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trf_tests`
--

INSERT INTO `trf_tests` (`id`, `trfId`, `testId`, `patientId`, `report`, `status`, `name`, `price`, `created_at`, `updated_at`) VALUES
(1, '1', '2', '1', 'Heart Beat Geeting High', '0', 'Maleria', '450', '2018-09-05 00:11:36', '2018-09-05 00:11:36'),
(2, '2', '1', '2', 'Duplicate Copy', '1', 'Jandis', '456', '2018-09-05 00:51:42', '2018-09-10 06:01:40'),
(3, '2', '2', '2', 'Heart Beat Geeting High', '1', 'Maleria', '500', '2018-09-05 00:51:42', '2018-09-10 06:04:32'),
(4, '2', '6', '2', 'Heavy Fever in Body', '1', 'Typoid', '11', '2018-09-05 00:51:42', '2018-09-10 06:04:43');

-- --------------------------------------------------------

--
-- Table structure for table `trves`
--

CREATE TABLE `trves` (
  `id` int(10) UNSIGNED NOT NULL,
  `patientId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doctorId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hospitalid` int(11) NOT NULL,
  `hospitalCC` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drawnDate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drawnTime` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receviedDate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receviedTime` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oldPathgene` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp9` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp11` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp12` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp13` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp14` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp15` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp16` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp17` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp18` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp19` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp20` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp21` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp22` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp23` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp24` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sp25` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `professionalDiagnosis` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hoMedication` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medicationStatus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `onGoingDuration` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terminatedWhen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fatingPeriod` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `HrUrineVolume` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LMP` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `diabticStatus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `geStation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachments` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tmpSentFrozen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tmpSentReFregator` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tmpSentAmbient` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tmpReceivedFrozen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tmpReceivedReFregator` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tmpReceivedAmbient` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trves`
--

INSERT INTO `trves` (`id`, `patientId`, `doctorId`, `hospitalid`, `hospitalCC`, `drawnDate`, `drawnTime`, `receviedDate`, `receviedTime`, `oldPathgene`, `sp1`, `sp2`, `sp3`, `sp4`, `sp5`, `sp6`, `sp7`, `sp8`, `sp9`, `sp10`, `sp11`, `sp12`, `sp13`, `sp14`, `sp15`, `sp16`, `sp17`, `sp18`, `sp19`, `sp20`, `sp21`, `sp22`, `sp23`, `sp24`, `sp25`, `professionalDiagnosis`, `hoMedication`, `medicationStatus`, `onGoingDuration`, `terminatedWhen`, `fatingPeriod`, `HrUrineVolume`, `LMP`, `diabticStatus`, `geStation`, `attachments`, `tmpSentFrozen`, `tmpSentReFregator`, `tmpSentAmbient`, `tmpReceivedFrozen`, `tmpReceivedReFregator`, `tmpReceivedAmbient`, `status`, `created_at`, `updated_at`) VALUES
(1, '1', '7', 2, '2', '1 September, 2018', NULL, '14 September, 2018', NULL, '9856', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', NULL, NULL, NULL, '895679', 'no', 'Ongoing', NULL, NULL, NULL, NULL, NULL, 'yes', 'no', NULL, '1', '1', '1', '1', '1', NULL, 0, '2018-09-05 00:11:36', '2018-09-05 00:11:36'),
(2, '2', '6', 1, '1', '6 September, 2018', NULL, '8 September, 2018', NULL, '89798', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', NULL, NULL, NULL, '89657', 'yes', 'Terminated', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '1', '1', '1', NULL, 2, '2018-09-05 00:51:42', '2018-09-05 00:51:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Rajesh', 'rajesh@vjsoft.org', '$2y$10$zB230Z5mt4MdIgMbBUO7FulDfM02etykbf97VFpsItpDSigWftA3K', 'RsHAUqWah8k0lyxsn4pEoIo67oOx4rWbSdDENSvo9H7jcnyXi2SecKpZLMy6', '2018-08-22 00:05:42', '2018-08-22 00:05:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `accounts_email_unique` (`email`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hospitals`
--
ALTER TABLE `hospitals`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `hospitals_email_unique` (`email`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trf_tests`
--
ALTER TABLE `trf_tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trves`
--
ALTER TABLE `trves`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `hospitals`
--
ALTER TABLE `hospitals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `trf_tests`
--
ALTER TABLE `trf_tests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `trves`
--
ALTER TABLE `trves`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
