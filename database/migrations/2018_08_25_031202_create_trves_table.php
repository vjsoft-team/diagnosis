<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrvesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trves', function (Blueprint $table) {
            $table->increments('id');
            $table->string('patientId');
            $table->string('doctorId');
            $table->string('hospitalCC')->nullable();
            $table->string('drawnDate')->nullable();
            $table->string('drawnTime')->nullable();
            $table->string('receviedDate')->nullable();
            $table->string('receviedTime')->nullable();
            $table->string('oldPathgene')->nullable();
            $table->string('sp1')->nullable();
            $table->string('sp2')->nullable();
            $table->string('sp3')->nullable();
            $table->string('sp4')->nullable();
            $table->string('sp5')->nullable();
            $table->string('sp6')->nullable();
            $table->string('sp7')->nullable();
            $table->string('sp8')->nullable();
            $table->string('sp9')->nullable();
            $table->string('sp10')->nullable();
            $table->string('sp11')->nullable();
            $table->string('sp12')->nullable();
            $table->string('sp13')->nullable();
            $table->string('sp14')->nullable();
            $table->string('sp15')->nullable();
            $table->string('sp16')->nullable();
            $table->string('sp17')->nullable();
            $table->string('sp18')->nullable();
            $table->string('sp19')->nullable();
            $table->string('sp20')->nullable();
            $table->string('sp21')->nullable();
            $table->string('sp22')->nullable();
            $table->string('sp23')->nullable();
            $table->string('sp24')->nullable();
            $table->string('sp25')->nullable();
            $table->string('professionalDiagnosis')->nullable();
            $table->string('hoMedication')->nullable();
            $table->string('medicationStatus')->nullable();
            $table->string('onGoingDuration')->nullable();
            $table->string('terminatedWhen')->nullable();
            $table->string('fatingPeriod')->nullable();
            $table->string('HrUrineVolume')->nullable();
            $table->string('LMP')->nullable();
            $table->string('diabticStatus')->nullable();
            $table->string('geStation')->nullable();
            $table->string('attachments')->nullable();
            $table->string('tmpSentFrozen')->nullable();
            $table->string('tmpSentReFregator')->nullable();
            $table->string('tmpSentAmbient')->nullable();
            $table->string('tmpReceivedFrozen')->nullable();
            $table->string('tmpReceivedReFregator')->nullable();
            $table->string('tmpReceivedAmbient')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trves');
    }
}
