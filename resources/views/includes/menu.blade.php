<div class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-dark navbar-without-dd-arrow navbar-shadow d-print-none" role="navigation" data-menu="menu-wrapper">
    <div class="navbar-container main-menu-content" data-menu="menu-container">
        <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">

            {{-- @if (Auth::user()->name != 'account') --}}
              @auth("web")

            <li class="nav-item">
                <a class="nav-link" href="{{ config('app.url') }}/home"><i class="la la-home"></i>
        <span>Dashboard</span>
               </a>
          </li>

          <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="la la-users"></i><span>Clients's</span></a>
              <ul class="dropdown-menu">
                  <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown">Hospital's</a>
                      <ul class="dropdown-menu">
                          <li data-menu=""><a class="dropdown-item" href="{{ config('app.url') }}/addHospital" data-toggle="dropdown">Add</a>
                          </li>
                          <li data-menu=""><a class="dropdown-item" href="{{ config('app.url') }}/listHospital" data-toggle="dropdown">List</a>
                          </li>
                          </li>
                      </ul>
                  </li>
                  <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown">Doctor's</a>
                      <ul class="dropdown-menu">
                          <li data-menu=""><a class="dropdown-item" href="{{ config('app.url') }}/addDoctor" data-toggle="dropdown">Add</a>
                          </li>
                          <li data-menu=""><a class="dropdown-item" href="{{ config('app.url') }}/listDoctor" data-toggle="dropdown">List</a>
                          </li>
                      </ul>
                  </li>
              </ul>
          </li>

          <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="la la-medkit"></i><span>Test's</span></a>
              <ul class="dropdown-menu">
                  <li data-menu="">
                      <a class="dropdown-item" href="{{ config('app.url') }}/addTest" data-toggle="dropdown"><i class="la la-copy"></i>Add</a>
                  </li>
                  <li data-menu="">
                      <a class="dropdown-item" href="{{ config('app.url') }}/listTest" data-toggle="dropdown"><i class="la la-copy"></i>List</a>
                  </li>
              </ul>
          </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ config('app.url') }}/trf" ><i class="la la-newspaper-o"></i>
                  <span>Trf</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ config('app.url') }}/trf-report" ><i class="la la-briefcase"></i>
                  <span>Trf Tests</span>
                </a>
            </li>
            @endauth
              @auth("admin")

            <li class="nav-item">
                <a class="nav-link" href="{{ config('app.url') }}/home"><i class="la la-home"></i>
        <span>Dashboard</span>
               </a>
          </li>

            <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="la la-users"></i><span>Clients's</span></a>
                <ul class="dropdown-menu">
                    <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown">Hospital's</a>
                        <ul class="dropdown-menu">
                            <li data-menu=""><a class="dropdown-item" href="{{ config('app.url') }}/addHospital" data-toggle="dropdown">Add</a>
                            </li>
                            <li data-menu=""><a class="dropdown-item" href="{{ config('app.url') }}/listHospital" data-toggle="dropdown">List</a>
                            </li>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown">Doctor's</a>
                        <ul class="dropdown-menu">
                            <li data-menu=""><a class="dropdown-item" href="{{ config('app.url') }}/addDoctor" data-toggle="dropdown">Add</a>
                            </li>
                            <li data-menu=""><a class="dropdown-item" href="{{ config('app.url') }}/listDoctor" data-toggle="dropdown">List</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>

            <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="la la-medkit"></i><span>Test's</span></a>
                <ul class="dropdown-menu">
                    <li data-menu="">
                        <a class="dropdown-item" href="{{ config('app.url') }}/addTest" data-toggle="dropdown"><i class="la la-copy"></i>Add</a>
                    </li>
                    <li data-menu="">
                        <a class="dropdown-item" href="{{ config('app.url') }}/listTest" data-toggle="dropdown"><i class="la la-copy"></i>List</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ config('app.url') }}/trf" ><i class="la la-newspaper-o"></i>
                  <span>Trf</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ config('app.url') }}/trf-report" ><i class="la la-briefcase"></i>
                  <span>Trf Tests</span>
                </a>
            </li>
            @endauth

          {{-- @endif --}}
            @auth("account")
                {{-- You're an administrator! --}}
                <li class="nav-item">
                    <a class="nav-link" href="{{ config('app.url') }}/account"><i class="la la-home"></i>
            <span>Dashboard</span>
                   </a>
              </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ config('app.url') }}/account/trf-completed" ><i class="la la-home"></i>
                    <span>Trf Completed</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ config('app.url') }}/account/pending-invoice" ><i class="la la-home"></i>
                    <span>Pending Invoices</span>
                  </a>
                </li>
                {{-- <li class="nav-item">
                  <a class="nav-link" href="{{ config('app.url') }}/trf-pending" ><i class="la la-home"></i>
                    <span>Trf Pending</span>
                  </a>
                </li> --}}
            @endauth

              @auth("hospital")
                <li class="nav-item">
                    <a class="nav-link" href="{{ config('app.url') }}/hospital"><i class="la la-home"></i>
            <span>Dashboard</span>
                   </a>
              </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ config('app.url') }}/hospital/reports"><i class="la la-home"></i>
                      <span>Invoice's</span>
                   </a>
                 </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ config('app.url') }}/hospital/reports-completed"><i class="la la-home"></i>
                      <span>Trfs's</span>
                   </a>
                 </li>
              @endauth
        </ul>
    </div>
</div>
