@php
  use App\trf_tests;
@endphp

<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
    <title>Diagnosis- Generate Test</title>
    <link rel="apple-touch-icon" href="{{ config('app.url') }}/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700" rel="stylesheet">
    <link href="{{ config('app.url') }}/line-awesome/css/line-awesome.min.css" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/vendors.min.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/app.min.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/core/colors/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/charts/jquery-jvectormap-2.0.3.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/charts/morris.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/fonts/simple-line-icons/style.min.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/core/colors/palette-gradient.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/assets/css/style.css">
    <!-- END Custom CSS-->
</head>

<body class="horizontal-layout horizontal-menu 2-columns   menu-expanded" data-open="hover" data-menu="horizontal-menu" data-col="2-columns">
    <!-- fixed-top-->
    @include('includes/nav')
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    @include('includes/menu')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Revenue, Hit Rate & Deals -->
                {{-- @php
                $new = Test::all();
                @endphp --}}



                <div class="row">
                  <div class="col-12">
                    <div class="card">
                      <div class="card-header">
                        <h4 class="card-title">Generate Test List


                        {{-- <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{ config('app.url') }}/home">Home</a>
                          </li>
                          <li class="breadcrumb-item"><a href="#">Clients</a>
                          </li>
                          <li class="breadcrumb-item active">Hospital List
                          </li>
                        </ol> --}}
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        {{-- <div class="heading-elements">
                          <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                          </ul>
                        </div> --}}
                      </div>
                      <div class="card-content collapse show">
                        {{-- <div class="card-body">
                          <p class="card-text">Add <code>.table-bordered</code> for borders on all sides of the
                            table and cells.</p>
                        </div> --}}
                        <div class="table-responsive">
                          <form class="" action="{{ route('payInvoice') }}" method="post">
                            {{ csrf_field() }}

                          <table class="table table-bordered mb-0">
                            <thead>
                              <tr>
                                <th>Sno</th>
                                <th>Pay</th>
                                <th>Test Id</th>
                                <th>Patient</th>
                                <th>Specimen Type</th>
                                <th>Test's</th>
                                <th>Progress</th>
                                <th>Amount</th>
                                {{-- <th>Action</th> --}}
                              </tr>
                            </thead>

                            <tbody>
                              @php
                                $trfs = DB::SELECT("SELECT * FROM trves WHERE hospitalid = $hospitalid AND status = 4");
                                $sno = 1;
                              @endphp
                              @foreach ($trfs as $trf)
                                <tr>
                                  <td>{{ $sno++ }}</td>
                                  <td><input type="checkbox" value="{{ $trf->id }}" name="trfs[]"></td>
                                  <td>PGTR{{ $trf->id }}</td>
                                  @php
                                    $patientId = $trf->patientId;
                                    $patientDetails = DB::SELECT("SELECT * FROM patients WHERE id = $patientId");
                                  @endphp
                                  <td>{{ $patientDetails[0]->patientName }}</td>
                                  <td></td>
                                  @php
                                    $trfId = $trf->id;
                                    $tests = DB::SELECT("SELECT * FROM trf_tests WHERE trfId = $trfId");
                                    $testCont = '';
                                    $testAmt = 0;
                                    foreach ($tests as $test) {
                                      // $testname = DB::SELECT("SELECT* FROM tests WHERE id = $test->testId");
                                      // dd($testname[0]->name);
                                      $testCont .= $test->name.', ';
                                      $testAmt += $test->price;
                                    }
                                  @endphp
                                  <td>{{ $testCont }}</td>
                                  <td>
                                  @if ($trf->status == 0)
                                    Received
                                  @elseif ($trf->status == 1)
                                    Processing
                                  @elseif ($trf->status == 2)
                                    Completed
                                  @elseif ($trf->status == 4)
                                    Payment Pending
                                  @elseif ($trf->status == 5)
                                    Finished
                                  @endif
                                  </td>
                                  <td>{{ $testAmt }}</td>
                                  {{-- <td><a href="invoice/{{ $trf->id }}">View</a></td> --}}
                                </tr>
                              @endforeach

                            </tbody>

                          </table>
                          <br>
                          <div class="col-md-1">
                            <input type="submit" name="update" value="Pay" class="btn btn-primary">
                          </div>
                          <br>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->

    @include('includes/footer')
    <!-- BEGIN VENDOR JS-->
    <script src="{{ config('app.url') }}/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script type="text/javascript" src="{{ config('app.url') }}/app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/app-assets/vendors/js/charts/jquery.sparkline.min.js"></script>

    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <script src="{{ config('app.url') }}/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script type="text/javascript" src="{{ config('app.url') }}/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js"></script>
    {{-- <script src="{{ config('app.url') }}/app-assets/js/scripts/pages/dashboard-sales.min.js" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL JS-->
</body>

</html>
