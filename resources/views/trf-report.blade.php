@php
  use App\Hospital;
  use App\trf;
@endphp

<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
    <title>Diagnosis- Hospitals List </title>
    <link rel="apple-touch-icon" href="{{ config('app.url') }}/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700" rel="stylesheet">
    <link href="{{ config('app.url') }}/line-awesome/css/line-awesome.min.css" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/app.min.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/core/colors/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/charts/jquery-jvectormap-2.0.3.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/charts/morris.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/fonts/simple-line-icons/style.min.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/core/colors/palette-gradient.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/assets/css/style.css">
    <!-- END Custom CSS-->
</head>

<body class="horizontal-layout horizontal-menu 2-columns   menu-expanded" data-open="hover" data-menu="horizontal-menu" data-col="2-columns">
    <!-- fixed-top-->
    @include('includes/nav')
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    @include('includes/menu')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Revenue, Hit Rate & Deals -->
                @php
                  $trfs = trf::all();
                  @endphp



                <div class="row">
                  <div class="col-12">
                    <div class="card">
                      <div class="card-header">
                        <h4 class="card-title">Tests List @if(Session::has('message'))
                            <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                           @endif</h4>
                        {{-- <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="{{ config('app.url') }}/home">Home</a>
                          </li>
                          <li class="breadcrumb-item"><a href="#">Clients</a>
                          </li>
                          <li class="breadcrumb-item active">Hospital List
                          </li>
                        </ol> --}}
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        {{-- <div class="heading-elements">
                          <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                          </ul>
                        </div> --}}
                      </div>
                      <div class="card-content collapse show">
                        {{-- <div class="card-body">
                          <p class="card-text">Add <code>.table-bordered</code> for borders on all sides of the
                            table and cells.</p>
                        </div> --}}
                        <div class="table-responsive">
                          <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th>S.No</th>
                          <th>Date</th>
                          <th>Trf ID</th>
                          <th>Patient Id</th>
                          <th>Patient Name</th>
                          <th>No. Of Tests</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @php
                          $sno = 1;
                        @endphp
                        @foreach ($trfs as $trf)
                          <tr>
                            <td>{{ $sno++ }}</td>
                            <td>{{ $trf->created_at }}</td>
                            <td>PGTR{{ $trf->id }}</td>
                            <td>PGP{{ $trf->patientId }}</td>
                            @php
                              $patientId = $trf->patientId;
                              $patientDetails = DB::select("SELECT * FROM patients WHERE id = '$patientId'");
                            @endphp
                            <td>{{ $patientDetails[0]->patientName }}</td>
                            @php
                              $testId = $trf->id;
                              $tests = DB::select("SELECT count(*) count FROM trf_tests where trfId = '$testId'");
                            @endphp
                            <td>{{ $tests[0]->count }}</td>
                            <td>
                            @if ($trf->status == 0)
                              Received
                            @elseif ($trf->status == 1)
                              Processing
                            @elseif ($trf->status == 2)
                              Completed
                            @endif
                            </td>
                            <td><a href="{{ config('app.url') }}/trf-report-view/{{ $trf->id }}">View</a></td>
                          </tr>
                        @endforeach
                      </tfoot>
                    </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->

    @include('includes/footer')
    <!-- BEGIN VENDOR JS-->
    <script src="{{ config('app.url') }}/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script type="text/javascript" src="{{ config('app.url') }}/app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/app-assets/vendors/js/charts/jquery.sparkline.min.js"></script>
    <script src="{{ config('app.url') }}/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <script src="{{ config('app.url') }}/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script type="text/javascript" src="{{ config('app.url') }}/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js"></script>
    <script src="{{ config('app.url') }}/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
</body>

</html>
