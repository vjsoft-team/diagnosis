<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
    <title>Diagnosis- Accounts
    </title>
    <link rel="apple-touch-icon" href="{{ config('app.url') }}/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700" rel="stylesheet">
    <link href="{{ config('app.url') }}/line-awesome/css/line-awesome.min.css" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/vendors.min.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/app.min.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/core/colors/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/charts/jquery-jvectormap-2.0.3.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/charts/morris.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/fonts/simple-line-icons/style.min.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/core/colors/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/cryptocoins/cryptocoins.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/assets/css/style.css">
    <!-- END Custom CSS-->
</head>

<body class="horizontal-layout horizontal-menu 2-columns   menu-expanded" data-open="hover" data-menu="horizontal-menu" data-col="2-columns">
    <!-- fixed-top-->
    @include('includes/nav')
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    @include('includes/menu')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
              <div class="col-md-1">
                <a href="{{ config('app.url') }}/account/generate">
                  <button type="button" class="btn btn-warning">Generate</button>
                </a>
              </div>
            </div>
        </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->

    @include('includes/footer')
    <!-- BEGIN VENDOR JS-->
    <script src="{{ config('app.url') }}/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script type="text/javascript" src="{{ config('app.url') }}/app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/app-assets/vendors/js/charts/jquery.sparkline.min.js"></script>
    <script src="{{ config('app.url') }}/app-assets/vendors/js/charts/chart.min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/vendors/js/charts/echarts/echarts.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/vendors/js/charts/raphael-min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/vendors/js/charts/morris.min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/data/jvector/visitor-data.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <script src="{{ config('app.url') }}/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script type="text/javascript" src="{{ config('app.url') }}/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js"></script>
    <script src="{{ config('app.url') }}/app-assets/js/scripts/pages/dashboard-sales.min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/js/scripts/pages/dashboard-crypto.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
</body>

</html>
{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
