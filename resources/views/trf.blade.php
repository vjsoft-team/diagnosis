@php use App\Hospital; use App\Doctor; @endphp
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="PIXINVENT">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>Diagnosis- Create TRF </title>
    <link rel="apple-touch-icon" href="{{ config('app.url') }}/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700" rel="stylesheet">
    <link href="{{ config('app.url') }}/line-awesome/css/line-awesome.min.css" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/forms/selects/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/forms/listbox/bootstrap-duallistbox.min.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/app.min.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/core/colors/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/charts/jquery-jvectormap-2.0.3.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/charts/morris.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/fonts/simple-line-icons/style.min.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/core/colors/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/plugins/forms/dual-listbox.min.css">
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/plugins/pickers/daterange/daterange.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/assets/css/style.css">
    <!-- END Custom CSS-->
</head>

<body class="horizontal-layout horizontal-menu 2-columns   menu-expanded" data-open="hover" data-menu="horizontal-menu" data-col="2-columns">
    <!-- fixed-top-->
    @include('includes/nav')
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    @include('includes/menu')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Revenue, Hit Rate & Deals -->
                {{-- Row 1 --}}
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="basic-layout-form">Create TRF</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    {{--
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul> --}}
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    {{--
                                    <div class="card-text">
                                    </div> --}}
                                    @php $hospital = Hospital::all(); @endphp

                                    <form class="form" action="{{ route('trfStore') }}" method="post">
                                        @csrf
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="ft-user"></i> Patients's Info</h4>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Patient Name<span class="text-danger">*</span></label>
                                                        <input type="text" name="patientName" id="projectinput1" class="form-control" placeholder="Patient Name" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                  <div class="form-group">
                                                    <label>Date of birth<span class="text-danger">*</span></label>
                                                    <div class="input-group">
                                                      <span class="input-group-text">
                                                        <span class="la la-calendar-o"></span>
                                                      </span>
                                                      <input type='text'  name="dob" class="form-control pickadate-selectors" placeholder="Month &amp; Year Selector"/ required>
                                                      <div class="input-group-append">
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="age">Age<span class="text-danger">*</span></label>
                                                        <input type="number" name="age" id="age" class="form-control" placeholder="Age" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="weight">Weight</label>
                                                        <input type="text"  name="weight" id="weight" class="form-control" placeholder="Weight" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="uid">UID</label>
                                                        <input type="text" name="uid" id="uid" class="form-control" placeholder="UID">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="contactNumber">Gender<span class="text-danger">*</span></label>
                                                        <select class="form-control" name="gender" required>
                                                            <option value="">Select</option>
                                                            <option value="M">Male</option>
                                                            <option value="F">Female</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label for="address">Address<span class="text-danger">*</span></label>
                                                        <input type="text" name="address" id="address" class="form-control" placeholder="Address" required>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="phone">Phone No<span class="text-danger">*</span></label>
                                                        <input type="text" name="phone" id="phone" class="form-control" placeholder="Phone No" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="height">Height</label>
                                                        <input type="text" name="height" id="height" class="form-control" placeholder="Height">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="doctorName">. </label>
                                                        <input type="text" name="doctorName" id="doctorName" class="form-control" placeholder="Cm" >
                                                    </div>
                                                </div>

                                            </div>
                                            {{--
                                            <div class="form-group">
                                                <label for="address">Address <span class="required text-danger">*</span></label>
                                                <textarea name="address" id="address" rows="5" class="form-control" placeholder="Address"></textarea>
                                            </div> --}}
                                        </div>

                                    {{-- </form> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="basic-layout-form">.</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    {{--
                                    <div class="card-text">
                                    </div> --}} @php $hospital = Hospital::all(); @endphp
                                    {{-- <form class="form" action="{{ route('addDoctor') }}" method="post">
                                        @csrf --}}
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="ft-user"></i>Referal Doctor</h4>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="doctorName">Doctor Name <span class="required text-danger">*</span></label>
                                                        @php $hospitals = DB::select("SELECT * FROM hospitals"); @endphp
                                                        <select class="select2 form-control" name="doctorName" onchange="gethospital(this)" required>
                                                            <option value="">Select</option>
                                                            @foreach ($hospitals as $hospital)
                                                            <optgroup label="{{ $hospital->name }}">
                                                                @php $hospital = $hospital->name; $doctors = DB::select("SELECT * FROM doctors WHERE hospital = '$hospital'"); @endphp @foreach ($doctors as $doctor)
                                                                <option value="{{ $doctor->id }}">{{ $doctor->doctorName }}</option>
                                                                @endforeach
                                                            </optgroup>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="hospitalName">Hospital Name <span class="required text-danger">*</span></label>
                                                        <input type="text" name="hospitalName" id="hospitalName" class="form-control" placeholder="Hospital Name" required>
                                                        <input type="hidden" name="hospitalid" id="hospitalid" class="form-control" placeholder="Hospital Name" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="hospitalPhone">Phone <span class="required text-danger">*</span></label>
                                                        <input type="text" name="hospitalPhone" id="hospitalPhone" class="form-control" placeholder="Hospital Phone" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="hospitalAddress">Address <span class="required text-danger">*</span></label>
                                                        <input type="text" name="hospitalAddress" id="hospitalAddress" class="form-control" placeholder="Hospital Address" required>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    {{-- </form> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Row 2 --}}
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">

                            <div class="card-content collapse show">
                                <div class="card-body">
                                    {{--
                                    <div class="card-text">
                                    </div> --}} @php $hospital = Hospital::all(); @endphp

                                        @csrf
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="ft-user"></i> Test's<span class="required text-danger">*</span></h4>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                  <div class="form-group">
                                                    @php
                                                      $tests = DB::select("SELECT * FROM `tests`");
                                                    @endphp
                                                    <select multiple="multiple" size="10" class="duallistbox" name="test[]" style="height: 375px" required>
                                                      @foreach ($tests as $test)
                                                        <option value="{{ $test->id }}">{{ $test->code }} | {{ $test->name }}</option>
                                                      @endforeach
                                                    </select>
                                                    {{-- <p class="mt-1">Add <code>.duallistbox</code> class for basic dual listbox.</p> --}}
                                                  </div>
                                                </div>
                                            </div>

                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">

                            <div class="card-content collapse show">
                                <div class="card-body">
                                    {{--
                                    <div class="card-text">
                                    </div> --}} @php $hospital = Hospital::all(); @endphp

                                        @csrf
                                        <div class="form-body">
                                          <h4 class="form-section"><i class="ft-user"></i> SPECIMEN INFORMATION</h4>
                                          <br>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="doctorName">HOSPITAL / CC <span class="required text-danger">*</span></label>
                                                        @php $hospitals = DB::select("SELECT * FROM hospitals"); @endphp
                                                        <select class="select2 form-control" name="hospitalCC "onchange="" required>
                                                            <option value="">Select</option>
                                                                @foreach ($hospitals as $hospital)
                                                                  <option value="{{ $hospital->id }}">{{ $hospital->name }}</option>
                                                                @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                      <label for="hospitalName">DRAWN DATE <span class="required text-danger">*</span></label>
                                                      <div class="input-group">
                                                        <div class="input-group-prepend">
                                                          <span class="input-group-text">
                                                            <span class="la la-calendar-o"></span>
                                                          </span>
                                                        </div>
                                                        <input type='text' name="drawnDate"class="form-control pickadate-short-string" placeholder="String Short of Month &amp; Week" / required>
                                                      </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                      <label for="hospitalPhone">DRAWN TIME <span class="required text-danger">*</span></label>
                                                      <div class="input-group">
                                                        <div class="input-group-prepend">
                                                          <span class="input-group-text">
                                                            <span class="ft-clock"></span>
                                                          </span>
                                                        </div>
                                                        <input type='text' name="drawnTime" class="form-control pickatime" placeholder="Basic Pick-a-time" / required>
                                                      </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                      <label for="hospitalName">Received DATE <span class="required text-danger">*</span></label>
                                                      <div class="input-group">
                                                        <div class="input-group-prepend">
                                                          <span class="input-group-text">
                                                            <span class="la la-calendar-o"></span>
                                                          </span>
                                                        </div>
                                                        <input type='text' name="receviedDate"class="form-control pickadate-short-string" placeholder="String Short of Month &amp; Week" / required>
                                                      </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                      <label for="hospitalPhone">Received TIME <span class="required text-danger">*</span></label>
                                                      <div class="input-group">
                                                        <div class="input-group-prepend">
                                                          <span class="input-group-text">
                                                            <span class="ft-clock"></span>
                                                          </span>
                                                        </div>
                                                        <input type='text' name="receviedTime" class="form-control pickatime" placeholder="Basic Pick-a-time" / required>
                                                      </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="oldPathgene">OLD PATHGENE LAB NO</label>
                                                        <input type="text" name="oldPathgene" id="oldPathgene" class="form-control" placeholder="OLD PATHGENE LAB NO">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">
                                {{-- <h4 class="card-title" id="basic-layout-form">SPECIMEN TYPE</h4> --}}
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    {{--
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul> --}}
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    {{--
                                    <div class="card-text">
                                    </div> --}}
                                    {{-- @php $hospital = Hospital::all(); @endphp --}}
                                    {{-- <form class="form" action="{{ route('addDoctor') }}" method="post">
                                        @csrf --}}
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="ft-user"></i> SPECIMEN TYPE</h4>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="serum">Serum</label>
                                                        <input type="text" name="serum" id="serum" class="form-control" placeholder="Serum">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="plasma">Plasma EDTA/FL/CIT/ACD</label>
                                                          <input type="text" name="plasma" id="plasma" class="form-control" placeholder="Plasma EDTA/FL/CIT/ACD">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="aspirate">FN Aspirate</label>
                                                        <input type="text" name="aspirate" id="aspirate" class="form-control" placeholder="FN Aspirate">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="bloodAcd">W.Blood ACD</label>
                                                        <input type="text" name="bloodAcd" id="bloodAcd" class="form-control" placeholder="W.Blood ACD">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="bloodEdta">W.Blood EDTA</label>
                                                        <input type="text" name="bloodEdta" id="bloodEdta" class="form-control" placeholder="W.Blood EDTA">
                                                    </div>
                                                  </div>
                                                    <div class="col-md-3">
                                                      <div class="form-group">
                                                          <label for="bloodFluoride">W.Blood Fluoride</label>
                                                          <input type="text" name="bloodFluoride" id="bloodFluoride" class="form-control" placeholder="W.Blood Fluoride">
                                                      </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                      <div class="form-group">
                                                          <label for="bloodSodiumCitrate">W.Blood Sodium Citrate</label>
                                                          <input type="text" name="bloodSodiumCitrate" id="bloodSodiumCitrate" class="form-control" placeholder="W.Blood Sodium Citrate">
                                                      </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                      <div class="form-group">
                                                          <label for="fluid">Fluid</label>
                                                          <input type="text" name="fluid" id="fluid" class="form-control" placeholder="Fluid">
                                                      </div>
                                                    </div>
                                                </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="sputum">Sputum</label>
                                                        <input type="text" name="sputum" id="sputum" class="form-control" placeholder="Sputum" >
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="filterPaper">Filter Paper</label>
                                                        <input type="text" name="filterPaper" id="filterPaper" class="form-control" placeholder="Filter Paper" >
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="boneMarrow">Bone Marrow</label>
                                                        <input type="text" name="boneMarrow" id="boneMarrow" class="form-control" placeholder="Bone Marrow" >
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="csf">CSF</label>
                                                        <input type="text" name="csf" id="csf" class="form-control" placeholder="CSF" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="tissueSmallMedium">Tissue-Small/Medium</label>
                                                        <input type="text" name="tissueSmallMedium" id="tissueSmallMedium" class="form-control" placeholder="Tissue-Small/Medium" >
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="tissueLarge">Tissue - Large</label>
                                                        <input type="text" name="tissueLarge" id="tissueLarge" class="form-control" placeholder="Tissue - Large" >
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="parafinBlock">Parafin Block</label>
                                                        <input type="text" name="parafinBlock" id="parafinBlock" class="form-control" placeholder="Parafin Block" >
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="smear">Smear</label>
                                                        <input type="text" name="smear" id="smear" class="form-control" placeholder="Smear" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="sideHE">Side (H&E)</label>
                                                        <input type="text" name="sideHE" id="sideHE" class="form-control" placeholder="Side (H&E)" >
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="urine">Urine 1st Morn/24 Hrs</label>
                                                        <input type="text" name="urine" id="urine" class="form-control" placeholder="Urine 1st Morn/24 Hrs" >
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="bal">BAL</label>
                                                        <input type="text" name="bal" id="bal" class="form-control" placeholder="BAL" >
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="stool">Stool</label>
                                                        <input type="text" name="stool" id="stool" class="form-control" placeholder="Stool" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="swab">Swab</label>
                                                        <input type="text" name="swab" id="swab" class="form-control" placeholder="Swab" >
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="others">Others</label>
                                                        <input type="text" name="others" id="others" class="form-control" placeholder="Others" >
                                                    </div>
                                                </div>
                                            </div>
                                            {{--
                                            <div class="form-group">
                                                <label for="address">Address <span class="required text-danger">*</span></label>
                                                <textarea name="address" id="address" rows="5" class="form-control" placeholder="Address"></textarea>
                                            </div> --}}
                                        </div>

                                    {{-- </form> --}}
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                {{-- <h4 class="card-title" id="basic-layout-form">SPECIMEN TYPE</h4> --}}
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    {{--
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul> --}}
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    {{--
                                    <div class="card-text">
                                    </div> --}}
                                    {{-- @php $hospital = Hospital::all(); @endphp --}}
                                    {{-- <form class="form" action="{{ route('addDoctor') }}" method="post">
                                        @csrf --}}
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="ft-user"></i>TEMPARATURE SENT</h4>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                      <label>
                                                        <input type="checkbox"  name="sFrozen" value="1" class="checkboxsas">Frozen - < -1o C
                                                      </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                  <div class="form-group">
                                                    <label>
                                                      <input type="checkbox"  name="sRefregerator" value="1" class="checkboxsas">Refregerator - 2 to 8o C
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                      <label>
                                                        <input type="checkbox"  name="sAmbient" value="1" class="checkboxsas">Ambient - 18 to 25o C
                                                      </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    {{-- </form> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="basic-layout-form">.</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    {{--
                                    <div class="card-text">
                                    </div> --}}
                                    {{-- @php $hospital = Hospital::all(); @endphp --}}

                                    {{-- <form class="form" action="{{ route('addDoctor') }}" method="post">
                                        @csrf --}}
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="ft-user"></i>ESSENTIAL CLINICAL INFORMATION</h4>
                                            <hr>

                                            <div class="row">
                                                <div class="col-md-12">
                                                  <div class="form-group">
                                                      <label for="proffesionalDiagnosis">PROFESSIONAL DIAGNOSIS</label>
                                                      <input type="text" name="proffesionalDiagnosis" id="proffesionalDiagnosis" class="form-control" placeholder="PROFESSIONAL DIAGNOSIS" >
                                                  </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="hoMedication">HO Medication</label>
                                                        <select class="select2 form-control" name="hoMedication ">
                                                            <option value="">Select</option>
                                                            <option value="yes">YES</option>
                                                            <option value="no">NO</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="status">Status of Medication</label>
                                                        <select class="select2 form-control" name="status ">
                                                            <option value="">Select</option>
                                                            <option value="Ongoing">Ongoing</option>
                                                            <option value="Terminated">Terminated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="onGoingDuration">If Ongoing, Duration</label>
                                                        <input type="text" name="onGoingDuration" id="onGoingDuration" class="form-control" placeholder="If Ongoing, Duration">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="terminatedWhen">If Terminated, When</label>
                                                        <input type="text" name="terminatedWhen" id="terminatedWhen" class="form-control" placeholder="If Terminated, When">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                  <div class="form-group">
                                                      <label for="fastingPeriod">Fasting Period</label>
                                                      <input type="text" name="fastingPeriod" id="fastingPeriod" class="form-control" placeholder="Fasting Period">
                                                  </div>
                                                </div>
                                                <div class="col-md-12">
                                                  <div class="form-group">
                                                      <label for="urineVolume">24 Hour Urine Volume</label>
                                                      <input type="text" name="urineVolume" id="urineVolume" class="form-control" placeholder="24 Hour Urine Volume">
                                                  </div>
                                                </div>
                                                <div class="col-md-12">
                                                  <div class="form-group">
                                                      <label for="lmp">LMP (Where Applicable)</label>
                                                      <input type="text" name="lmp" id="lmp" class="form-control" placeholder="LMP (Where Applicable)">
                                                  </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="diabeticStatus">Diabetic Status</label>
                                                        <select class="select2 form-control" name="diabeticStatus ">
                                                            <option value="">Select</option>
                                                            <option value="yes">YES</option>
                                                            <option value="no">NO</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="gestation">Gestation</label>
                                                        <select class="select2 form-control" name="gestation ">
                                                            <option value="">Select</option>
                                                            <option value="yes">YES</option>
                                                            <option value="no">NO</option>
                                                        </select>
                                                    </div>
                                              </div>
                                              <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="file">Attachments</label>
                                                    <input type="file" name="file" id="file" class="form-control" placeholder="Upload" >
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    {{-- </form> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                {{-- <h4 class="card-title" id="basic-layout-form">SPECIMEN TYPE</h4> --}}
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    {{--
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul> --}}
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    {{--
                                    <div class="card-text">
                                    </div> --}}
                                    {{-- @php $hospital = Hospital::all(); @endphp --}}
                                    {{-- <form class="form" action="{{ route('addDoctor') }}" method="post">
                                        @csrf --}}
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="ft-user"></i>TEMPARATURE RECEIVED</h4>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                      <label>
                                                        <input type="checkbox"  name="rFrozen" value="1" class="checkboxsas">Frozen - < -1o C
                                                      </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                  <div class="form-group">
                                                    <label>
                                                      <input type="checkbox"  name="rRefregerator" value="1" class="checkboxsas">Refregerator - 2 to 8o C
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                      <label>
                                                        <input type="checkbox"  name="rAmbient" value="1" class="checkboxsas">Ambient - 18 to 25o C
                                                      </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-success">
                                              <i class="la la-check-square-o"></i> Create TRF
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->

    @include('includes/footer')
    <!-- BEGIN VENDOR JS-->
    <script src="{{ config('app.url') }}/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script type="text/javascript" src="{{ config('app.url') }}/app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/app-assets/vendors/js/charts/jquery.sparkline.min.js"></script>
    <script src="{{ config('app.url') }}/app-assets/vendors/js/charts/chart.min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/vendors/js/charts/raphael-min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/vendors/js/charts/morris.min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/data/jvector/visitor-data.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/vendors/js/forms/listbox/jquery.bootstrap-duallistbox.min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/vendors/js/pickers/daterange/daterangepicker.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN MODERN JS-->
    <script src="{{ config('app.url') }}/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script type="text/javascript" src="{{ config('app.url') }}/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js"></script>
    <script src="{{ config('app.url') }}/app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/js/scripts/forms/listbox/form-duallistbox.min.js" type="text/javascript"></script>
    <script src="{{ config('app.url') }}/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js" type="text/javascript"></script>
    {{--
    <script src="{{ config('app.url') }}/app-assets/js/scripts/pages/dashboard-sales.min.js" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL JS-->

    <script type="text/javascript">
        function gethospital(e) {
            var doctorId = e.value;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: '/api/getHospitals',
                data: {
                    doctorId: doctorId
                },
                success: function(data) {
                    console.log(data[0].id);
                    $('#hospitalName').val(data[0].name);
                    $('#hospitalid').val(data[0].id);
                    $('#hospitalAddress').val(data[0].address);
                    $('#hospitalPhone').val(data[0].contactNumber);
                }
            });
        }
    </script>
</body>

</html>
