@php
use App\Test;
use App\Hospital;
use App\trf;
use App\trf_tests;
use App\patient;
use App\Doctor;
@endphp
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta name="_token" content="{{csrf_token()}}" />
      <title>Diagnosis- Hospitals List
      </title>
      <link rel="apple-touch-icon" href="{{ config('app.url') }}/app-assets/images/ico/apple-icon-120.png">
      <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/images/ico/favicon.ico">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700" rel="stylesheet">
      <link href="{{ config('app.url') }}/line-awesome/css/line-awesome.min.css" rel="stylesheet">
      <!-- BEGIN VENDOR CSS-->
      <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/vendors.min.css">
      {{-- <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/editors/quill/katex.min.css">
      <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/editors/quill/monokai-sublime.min.css">
      <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/editors/quill/quill.snow.css">
      <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/editors/quill/quill.bubble.css">
      <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/pickers/daterange/daterangepicker.css"> --}}
      {{-- <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/tables/datatable/datatables.min.css"> --}}
      <!-- END VENDOR CSS-->
      <!-- BEGIN MODERN CSS-->
      <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/app.min.css">
      <!-- END MODERN CSS-->
      <!-- BEGIN Page Level CSS-->
      <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
      <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/core/colors/palette-gradient.min.css">
      <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/pages/invoice.min.css">
      {{-- <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/plugins/pickers/daterange/daterange.min.css"> --}}
      <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
      <!-- END Page Level CSS-->
      <!-- BEGIN Custom CSS-->
      <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/assets/css/style.css">
      <!-- END Custom CSS-->
   </head>
   <body class="horizontal-layout horizontal-menu 2-columns   menu-expanded" data-open="hover" data-menu="horizontal-menu" data-col="2-columns">
      <!-- fixed-top-->
      @include('includes/nav')
      <!-- ////////////////////////////////////////////////////////////////////////////-->
      @include('includes/menu')
      <div class="app-content content">
         <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
               <!-- Revenue, Hit Rate & Deals -->
               <div class="row">
                  <div class="col-md-12 col-sm-12">
                     <div class="card text-white box-shadow-0 bg-info">
                        <div class="card-header" style="padding-bottom: 0px;">
                           <h4 class="card-title text-white">Patient Details</h4>
                           {{-- <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a> --}}
                        </div>
                        @php
                          $trf = trf::find($trfId);
                        @endphp
                        <div class="card-content collapse show">
                           <div class="card-body">
                              <div class="row">
                                 <div class="col-md-4">
                                    <table class="table table-column">
                                       <tbody>
                                          <tr>
                                             <th>patient Id</th>
                                             <td>PGP{{ $trf->patientId }}</td>
                                          </tr>
                                          @php
                                            $patientId = $trf->patientId;
                                            $patientDetails = patient::find($patientId);
                                          @endphp
                                          <tr>
                                             <th>patient Name</th>
                                             <td><b>{{ $patientDetails->patientName }}</b></td>
                                          </tr>
                                          <tr>
                                             <th>Phone</th>
                                             <td>{{ $patientDetails->phone }}</td>
                                          </tr>
                                          <tr>
                                             <th>Gender</th>
                                             <td>{{ $patientDetails->gender }}</td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                                 <div class="col-md-3">
                                   @php
                                     $doctor = Doctor::find($trf->doctorId);
                                   @endphp
                                    <table class="table table-column">
                                       <tbody>
                                          <tr>
                                             <th>DOCTOR NAME</th>
                                             <td>{{ $doctor->doctorName }}</td>
                                          </tr>
                                          <tr>
                                             <th>HOSPITAL</th>
                                             <td>{{ $doctor->hospital }}</td>
                                          </tr>
                                          <tr>
                                             <th>PHONE</th>
                                             <td>{{ $doctor->contactNumber }}</td>
                                          </tr>
                                          <tr>
                                             <th>Address</th>
                                             <td></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                                 <div class="col-md-4">
                                    <table class="table table-column">
                                       <tbody>
                                          <tr>
                                             <th>HOSPITAL/CC</th>
                                             <td>{{ $trf->hospitalCC }}</td>
                                          </tr>
                                          <tr>
                                             <th>DRAWN DATE</th>
                                             @php
                                               $drawnDate = Carbon\Carbon::createFromFormat('d F, Y', $trf->drawnDate)->format('d-m-Y');
                                             @endphp
                                             <td>{{ $drawnDate }} {{ $trf->drawnTime }} </td>
                                          </tr>
                                          <tr>
                                             <th>RECEIVED DATE</th>
                                             @php
                                               $receviedDate = Carbon\Carbon::createFromFormat('d F, Y', $trf->receviedDate)->format('d-m-Y');
                                             @endphp
                                             <td>{{ $receviedDate }}</td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                              {{-- Reference Doctor --}}
                              {{-- SPECIMEM INFORMATION --}}
                           </div>
                        </div>
                     </div>

                     <div class="card" style="">
                <div class="card-header">
                  <h4 class="card-title" id="basic-layout-form">Add Report</h4>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                    <form class="form">
                      <div class="form-body">
                        <div class="row">
                          <div class="col-md-2">
                            <div class="form-group">
                              <label for="projectinput1">Test</label>
                              @php
                                $trfIdNew = $trfId;
                                $tests = DB::select("SELECT * FROM trf_tests WHERE trfId = '$trfIdNew'");
                                // dd($tests);
                              @endphp
                              <select class="form-control" name="test" id="test" onchange="loadformat(this)">
                                <option value="">Select</option>

                                @foreach ($tests as $test)
                                  @php
                                    $testDetails = Test::find($test->testId);
                                    // dd($testDetails);
                                  @endphp
                                  <option value="{{ $test->id }}" @if ($test->status == 1)
                                    disabled
                                  @endif>{{ $testDetails->code }}</option>
                                @endforeach
                              </select>
                              <br>
                              <button type="button" id="updateReport" name="button" class="btn btn-primary">Save Report</button>
                              {{-- <input type="text" id="projectinput1" class="form-control" placeholder="First Name" name="fname"> --}}
                            </div>
                          </div>
                          <div class="col-md-9">
                            <div class="form-group">
                              <label for="projectinput2">Report</label>
                              <div id="full-wrapper">
                                <div id="full-container">
                                  <textarea name="report" class="summernote" id='summernote'></textarea>
                                  {{-- <div class="editor" id="textBox">
                                  </div>
                                  <textarea type="hidden" style="display: none;" id="hiddeninput" name="report"></textarea> --}}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      {{-- <div class="form-actions">
                        <button type="button" class="btn btn-warning mr-1">
                          <i class="ft-x"></i> Cancel
                        </button>
                        <button type="submit" class="btn btn-primary" id="save">
                          <i class="la la-check-square-o"></i> Save
                        </button>
                      </div> --}}
                    </form>
                  </div>
                </div>
              </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- ////////////////////////////////////////////////////////////////////////////-->
      @include('includes/footer')
      <!-- BEGIN VENDOR JS-->
      <script src="{{ config('app.url') }}/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
      <!-- BEGIN VENDOR JS-->
      <!-- BEGIN PAGE VENDOR JS-->
      <script type="text/javascript" src="{{ config('app.url') }}/app-assets/vendors/js/ui/jquery.sticky.js"></script>
      <script type="text/javascript" src="{{ config('app.url') }}/app-assets/vendors/js/charts/jquery.sparkline.min.js"></script>
      {{-- <script src="{{ config('app.url') }}/app-assets/vendors/js/editors/quill/katex.min.js" type="text/javascript"></script>
      <script src="{{ config('app.url') }}/app-assets/vendors/js/editors/quill/highlight.min.js" type="text/javascript"></script>
      <script src="{{ config('app.url') }}/app-assets/vendors/js/editors/quill/quill.min.js" type="text/javascript"></script>
      <script src="{{ config('app.url') }}/app-assets/vendors/js/extensions/jquery.steps.min.js" type="text/javascript"></script>
      <script src="{{ config('app.url') }}/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js" type="text/javascript"></script>
      <script src="{{ config('app.url') }}/app-assets/vendors/js/pickers/daterange/daterangepicker.js" type="text/javascript"></script>
      <script src="{{ config('app.url') }}/app-assets/vendors/js/forms/validation/jquery.validate.min.js" type="text/javascript"></script> --}}
      {{-- <script src="{{ config('app.url') }}/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script> --}}
      <!-- END PAGE VENDOR JS-->
      <!-- BEGIN MODERN JS-->
      <script src="{{ config('app.url') }}/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
      <script src="{{ config('app.url') }}/app-assets/js/core/app.min.js" type="text/javascript"></script>
      <script src="{{ config('app.url') }}/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
      <!-- END MODERN JS-->
      <!-- BEGIN PAGE LEVEL JS-->
      <script type="text/javascript" src="{{ config('app.url') }}/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js"></script>
      {{-- <script src="{{ config('app.url') }}/app-assets/js/scripts/editors/editor-quill.min.js" type="text/javascript"></script> --}}
      <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
      <!-- END PAGE LEVEL JS-->
      <script type="text/javascript">
      $(document).ready(function() {
        $('#summernote').summernote();
      });
      // $(function(){
      //   $('#save').click(function () {
      //       var mysave = $('#textBox').html();
      //       $('#hiddeninput').val(mysave);
      //   });
      // });
      </script>
      <script type="text/javascript">

        $(function(){
          $('#updateReport').click(function () {
              // var mysave = $('#textBox').html();
              // $('#hiddeninput').val(mysave);
              var updateId = $('#test').val();
              var updateReportValue = $('#summernote').val();
              $.ajax({
                headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
              type: "POST",
              url: '/api/updateReport',
              data: {updateReportValue: updateReportValue, updateId: updateId},
                success: function( data ) {
                    console.log(data);
                    // $('#textBox').html(data);
                    alert('report updated successfully')
                }
              });
          });
        });
      </script>
      <script type="text/javascript">
        function loadformat(e) {
            var testId = e.value;
            $.ajax({
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            type: "POST",
            url: '/api/getTestFormat',
            data: {testId: testId},
              success: function( data ) {
                $("#summernote").summernote("code", data);
                // $('#summernote').val('llll');
                // document.getElementById('summernote').value = 'lll';
                  console.log(data);
              }
            });
        }
      </script>
   </body>
</html>
