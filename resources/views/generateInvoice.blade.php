@php
use App\Test;
use App\Hospital;
use App\trf;
use App\trf_tests;
use App\patient;
use App\Doctor;
@endphp
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
      <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
      <meta name="author" content="PIXINVENT">
      <title>Diagnosis- Invoice View
      </title>
      <link rel="apple-touch-icon" href="{{ config('app.url') }}/app-assets/images/ico/apple-icon-120.png">
      <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/images/ico/favicon.ico">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700" rel="stylesheet">
      <link href="{{ config('app.url') }}/line-awesome/css/line-awesome.min.css" rel="stylesheet">
      <!-- BEGIN VENDOR CSS-->
      <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/vendors.min.css">
      <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/vendors/css/tables/datatable/datatables.min.css">
      <!-- END VENDOR CSS-->
      <!-- BEGIN MODERN CSS-->
      <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/app.min.css">
      <!-- END MODERN CSS-->
      <!-- BEGIN Page Level CSS-->
      <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
      <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/core/colors/palette-gradient.min.css">
      <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/app-assets/css/pages/invoice.min.css">
      <!-- END Page Level CSS-->
      <!-- BEGIN Custom CSS-->
      <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/assets/css/style.css">
      <!-- END Custom CSS-->
   </head>
   <body class="horizontal-layout horizontal-menu 2-columns   menu-expanded" data-open="hover" data-menu="horizontal-menu" data-col="2-columns">
      <!-- fixed-top-->
      @include('includes/nav')
      <!-- ////////////////////////////////////////////////////////////////////////////-->
      @include('includes/menu')
      <div class="app-content content">
         <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
               <!-- Revenue, Hit Rate & Deals -->
               <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-content collapse show">
                           <div id="invoice-template" class="card-body">
                              <!-- Invoice Company Details -->
                              @php
                                $trf = trf::find($id);
                              @endphp
                              <div id="invoice-company-details" class="row">
                                 <div class="col-md-12 text-center">
                                    <div class="media">
                                       {{-- <img src="../../../app-assets/images/logo/logo-80x80.png" alt="company logo" class=""
                                          /> --}}

                                       <div class="media-body">
                                          <ul class="ml-2 px-0 list-unstyled">
                                             <h3 class="content-header-title">DIAGNOSIS HEALTH CARE PVT LTD</h3>
                                             <li>TIRUPATHI,</li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-12 text-center text-md-right">

                                    {{-- <a href="{{ config('app.url') }}/trf-add-report/{{ $id }}" class="btn btn-info btn-lg my-1"><i class="la la-paper-plane-o"></i> Add Report</a> --}}
                                       {{-- <button type="button" class="btn btn-info btn-lg my-1"><i class="la la-paper-plane-o"></i> Add Report</button> --}}

                                    {{-- <h2>INVOICE</h2>
                                    <p class="pb-3"># INV-001001</p> --}}
                                    <ul class="px-0 list-unstyled">
                                       {{-- <li>Balance Due</li>
                                       <li class="lead text-bold-800">$ 12,000.00</li> --}}
                                    </ul>
                                 </div>
                              </div>
                              <!--/ Invoice Company Details -->
                              <!-- Invoice Customer Details -->
                              <div id="invoice-customer-details" class="row pt-2">
                                <div class="col-md-6 col-sm-12 text-center text-md-left">
                                   <p>
                                      <span class="text-muted">TRF ID :</span> PGHLP{{ $trf->id }}
                                   </p>
                                   @php
                                     $hospitalId = $trf['hospitalid'];
                                     $hospitalDetails = DB::select("SELECT * FROM hospitals WHERE id = $hospitalId");
                                     // return dd($hospitalDetails[0]->name);
                                   @endphp
                                   <p>
                                      <span class="text-muted">CC NAME :</span> {{ $hospitalDetails[0]->name }}
                                   </p>
                                   <p>
                                      <span class="text-muted">CC ADDRESS :</span> {{ $hospitalDetails[0]->address }}
                                   </p>
                                </div>
                                {{-- <div class="col-md-5 col-sm-12 text-md-right"> --}}
                                {{-- </div> --}}
                                 <div class="col-md-6 col-sm-12 text-center text-md-right">
                                   <button type="button" class="btn btn-info btn-lg my-1" data-toggle="modal" data-target="#bootstrap"><i class="la la-paper-plane-o"></i> Edit</button>
                                   <div class="modal fade text-left" id="bootstrap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35"
                          aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h3 class="modal-title" id="myModalLabel35"> Modal Title</h3>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <form method="post" action="{{ route('account.priceChange') }}">
                                  {{ csrf_field() }}
                                  <div class="modal-body">
                                    @php
                                    $tests = DB::select("SELECT * FROM trf_tests WHERE trfId = $trf->id");
                                    $totalAmount = 0;
                                  @endphp
                                  @foreach ($tests as $test)
                                    <fieldset class="form-group floating-label-form-group">
                                      <label for="email">{{ $test->name }}</label>
                                      <input type="text" class="form-control" placeholder="Price" name="price[]" value="{{ $test->price }}">
                                    </fieldset>
                                  @endforeach

                                  <input type="hidden" name="trfId" value="{{ $trf->id }}">

                                  </div>
                                  <div class="modal-footer">
                                    <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal"
                                    value="close">
                                    <input type="submit" class="btn btn-outline-primary btn-lg" value="Save">
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>

                                    {{-- <p>
                                       <span class="text-muted">INNVOICE NUMBER :</span> 06/05/2017
                                    </p> --}}
                                    <p>
                                       <span class="text-muted">INVOICE DATE :</span> {{ substr($trf->created_at, 0, 10) }}
                                    </p>
                                    @php
                                      $month = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $trf->created_at)->format('F');
                                    @endphp
                                    <p>
                                       <span class="text-muted">MONTH :</span> {{ $month }}
                                    </p>
                                 </div>
                              </div>
                              <!--/ Invoice Customer Details -->
                              <!-- Invoice Items Details -->
                              <div id="invoice-items-details" class="pt-2">
                                 <div class="row">
                                    <div class="table-responsive col-sm-12">
                                       <table class="table">
                                          <thead>
                                             <tr>
                                                <th>Date</th>
                                                <th>Patient Name</th>
                                                <th class="text-right">Age/Sex</th>
                                                <th class="text-right">Test Name</th>
                                                <th class="text-right">Amount</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                            @php
                                              $tests = DB::select("SELECT * FROM trf_tests WHERE trfId = $trf->id");
                                              $totalAmount = 0;
                                            @endphp
                                            @foreach ($tests as $test)
                                              <tr>
                                                <td>{{ substr($test->created_at, 0, 10) }}</td>
                                                @php
                                                  $patientDetails = DB::select("SELECT * FROM patients WHERE id = $test->patientId")
                                                @endphp
                                                <td> <p>{{ $patientDetails[0]->patientName }}</p></td>
                                                @php
                                                  $gender = '';
                                                  if ($patientDetails[0]->gender == 'M') {
                                                    $gender = "Male";
                                                  } elseif ($patientDetails[0]->gender == 'F') {
                                                    $gender = "Female";
                                                  }
                                                @endphp
                                                <td class="text-right">{{ $patientDetails[0]->age }} / {{ $gender }}</td>
                                                @php
                                                  // $testDetails = DB::select("SELECT * FROM tests WHERE id = $test->testId");
                                                @endphp
                                                <td class="text-right">{{ $test->name }}</td>
                                                <td class="text-right">{{ $test->price }}</td>
                                                @php
                                                  $totalAmount += $test->price;
                                                @endphp
                                              </tr>
                                            @endforeach
                                             {{-- <tr>
                                                <th scope="row">2</th>
                                                <td>
                                                   <p>iOS Application Development</p>
                                                   <p class="text-muted">Pellentesque maximus feugiat lorem at cursus.</p>
                                                </td>
                                                <td class="text-right">$ 25.00/hr</td>
                                                <td class="text-right">260</td>
                                                <td class="text-right">$ 6500.00</td>
                                             </tr> --}}
                                             {{-- <tr>
                                                <th scope="row">3</th>
                                                <td>
                                                   <p>WordPress Template Development</p>
                                                   <p class="text-muted">Vestibulum euismod est eu elit convallis.</p>
                                                </td>
                                                <td class="text-right">$ 20.00/hr</td>
                                                <td class="text-right">300</td>
                                                <td class="text-right">$ 6000.00</td>
                                             </tr> --}}
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <div class="row">
                                   <div class="col-sm-12">
                                      {{-- <h6>Terms & Condition</h6> --}}
                                      <p>KINDLY REMIT THE TOTAL PAYABLE AMOUNT RS (Rs. {{ $totalAmount }}/-) AT THE EARLIEST BY A/C. BANK DRAFTS OR CHEQUE IN FAVOUR OF DIAGNOSIS HEALTHCARE PVT LTD PAYABLE AT TIRUPATHI.
                                        OR CAN REMIT DIRECTLY INTO OUR ORIENTAL BANK OF CAMMERCE, PMR PLAZA, TIRUPATI </p>
                                   </div>
                                 </div>
                                 <div class="row">
                                    {{-- <div class="col-md-7 col-sm-12 text-center text-md-left"> --}}
                                      <div class="col-md-7 col-sm-12 text-center text-md-left">
                                         <p>
                                            <span class="text-muted">ACCOUNT NUMBER:</span> 85596996666
                                         </p>
                                         <p>
                                            <span class="text-muted">IFSC CODE:</span> ICIC000045
                                         </p>
                                      </div>
                                       {{-- <p class="lead">Payment Methods:</p> --}}
                                       {{-- <div class="row">
                                          <div class="col-md-8">
                                             <table class="table table-borderless table-sm">
                                                <tbody>
                                                   <tr>
                                                      <td>Account Number:</td>
                                                      <td>ABC Bank, USA</td>
                                                   </tr>
                                                   <tr>
                                                      <td>IFSC Code:</td>
                                                      <td>FGS165461646546AA</td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                       </div> --}}
                                    {{-- </div> --}}
                                    <div class="col-md-5 col-sm-12">
                                       {{-- <p class="lead">Total due</p> --}}
                                       {{-- <div class="table-responsive">
                                          <table class="table">
                                             <tbody>
                                                <tr>
                                                   <td>Sub Total</td>
                                                   <td class="text-right">$ 14,900.00</td>
                                                </tr>
                                                <tr>
                                                   <td>TAX (12%)</td>
                                                   <td class="text-right">$ 1,788.00</td>
                                                </tr>
                                                <tr>
                                                   <td class="text-bold-800">Total</td>
                                                   <td class="text-bold-800 text-right"> $ 16,688.00</td>
                                                </tr>
                                                <tr>
                                                   <td>Payment Made</td>
                                                   <td class="pink text-right">(-) $ 4,688.00</td>
                                                </tr>
                                                <tr class="bg-grey bg-lighten-4">
                                                   <td class="text-bold-800">Balance Due</td>
                                                   <td class="text-bold-800 text-right">$ 12,000.00</td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </div> --}}
                                       <div class="text-md-right">
                                          <p>Your's Truly</p>
                                          {{-- <img src="{{ config('app.url') }}/app-assets/images/pages/signature-scan.png" alt="signature" class="height-100"/> --}}
                                          <p class="height-50"></p>
                                          <p class="text-muted">(Account Officer)</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- Invoice Footer -->
                              <div id="invoice-footer">
                                 <div class="row">
                                    <div class="col-md-7 col-sm-12">
                                       {{-- <h6>Terms & Condition</h6> --}}
                                       <p></p>
                                    </div>
                                    {{-- <div class="col-md-5 col-sm-12 text-md-right">
                                       <button type="button" class="btn btn-info btn-lg my-1"><i class="la la-paper-plane-o"></i> Send Invoice</button>
                                    </div> --}}
                                 </div>
                              </div>
                              <!--/ Invoice Footer -->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- ////////////////////////////////////////////////////////////////////////////-->
      @include('includes/footer')
      <!-- BEGIN VENDOR JS-->
      <script src="{{ config('app.url') }}/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
      <!-- BEGIN VENDOR JS-->
      <!-- BEGIN PAGE VENDOR JS-->
      <script type="text/javascript" src="{{ config('app.url') }}/app-assets/vendors/js/ui/jquery.sticky.js"></script>
      <script type="text/javascript" src="{{ config('app.url') }}/app-assets/vendors/js/charts/jquery.sparkline.min.js"></script>
      <script src="{{ config('app.url') }}/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
      <!-- END PAGE VENDOR JS-->
      <!-- BEGIN MODERN JS-->
      <script src="{{ config('app.url') }}/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
      <script src="{{ config('app.url') }}/app-assets/js/core/app.min.js" type="text/javascript"></script>
      <script src="{{ config('app.url') }}/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
      <!-- END MODERN JS-->
      <!-- BEGIN PAGE LEVEL JS-->
      <script type="text/javascript" src="{{ config('app.url') }}/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js"></script>
      <script src="{{ config('app.url') }}/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
      <!-- END PAGE LEVEL JS-->
   </body>
</html>
